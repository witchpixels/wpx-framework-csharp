#if TOOLS
using Godot;
using Witchpixels.Framework.Entities;
using Witchpixels.Framework.Entities.Entity2D;
using Witchpixels.Framework.Entities.Entity3D;

namespace Witchpixels.Framework;

[Tool]
public partial class PluginMain : EditorPlugin
{
    public override void _EnterTree()
    {
        var entityNodeIcon =
            GD.Load<Texture2D>("res://addons/Witchpixels.Framework/assets/entities/node_entity_icon.png");
        var entityNode2DIcon =
            GD.Load<Texture2D>("res://addons/Witchpixels.Framework/assets/entities/node2d_entity_icon.png");
        var entityNode3DIcon =
            GD.Load<Texture2D>("res://addons/Witchpixels.Framework/assets/entities/node3d_entity_icon.png");

        var nodeEntityScript = GD.Load<Script>("res://addons/Witchpixels.Framework/src/Entities/NodeEntity.cs");
        var node3DEntityScript =
            GD.Load<Script>("res://addons/Witchpixels.Framework/src/Entities/Entity3D/Node3DEntity.cs");
        var character3DEntityScript =
            GD.Load<Script>("res://addons/Witchpixels.Framework/src/Entities/Entity3D/Character3DEntity.cs");

        var node2DEntityScript =
            GD.Load<Script>("res://addons/Witchpixels.Framework/src/Entities/Entity2D/Node2DEntity.cs");
        var character2DEntityScript =
            GD.Load<Script>("res://addons/Witchpixels.Framework/src/Entities/Entity2D/Character2DEntity.cs");

        AddCustomType(nameof(NodeEntity), nameof(Node), nodeEntityScript, entityNodeIcon);
        AddCustomType(nameof(Node3DEntity), nameof(Node3D), node3DEntityScript, entityNode3DIcon);
        AddCustomType(nameof(Character3DEntity), nameof(CharacterBody3D), character3DEntityScript, entityNode3DIcon);
        AddCustomType(nameof(Node2DEntity), nameof(Node2D), node2DEntityScript, entityNode2DIcon);
        AddCustomType(nameof(Character2DEntity), nameof(CharacterBody2D), character2DEntityScript, entityNode2DIcon);
    }

    public override void _ExitTree()
    {
        RemoveCustomType(nameof(NodeEntity));
        RemoveCustomType(nameof(Node3DEntity));
        RemoveCustomType(nameof(Character3DEntity));
        RemoveCustomType(nameof(Node2DEntity));
        RemoveCustomType(nameof(Character2DEntity));
    }
}
#endif