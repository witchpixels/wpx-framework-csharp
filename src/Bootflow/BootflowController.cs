using Godot;
using Witchpixels.Framework.Diagnostics.Logging;
using Witchpixels.Framework.Injection;

namespace Witchpixels.Framework.Bootflow;

public partial class BootflowController : Control
{
    private int _childCount;

    private int _childIndex;
    [Export(PropertyHint.File)] private string _initialScenePath = string.Empty;

    private ILogger _logger;
    [Export(PropertyHint.Range, "0,5")] private float _staticImageFadeInTimeInSeconds = 0.5f;
    [Export(PropertyHint.Range, "0,5")] private float _staticImageFadeOutTimeInSeconds = 1f;
    [Export(PropertyHint.Range, "0,10")] private float _staticImageLingerTimeInSeconds = 1f;

    public override void _Ready()
    {
        base._Ready();

        _logger = GlobalServiceContainer.ResolveService<ILoggerFactory>().CreateInstanceLogger(this);
        _childCount = GetChildCount();

        _logger.Debug("Bootflow has {0} cards", _childCount);

        for (var i = 0; i < _childCount; ++i)
        {
            var child = GetChild(i);
            if (child is CanvasItem c) c.Visible = false;

            if (child is VideoStreamPlayer videoStreamPlayer)
            {
                videoStreamPlayer.Autoplay = false;
                videoStreamPlayer.Paused = true;
                videoStreamPlayer.Loop = false;
            }
        }

        StartNextSlide();
    }

    private void StartNextSlide()
    {
        if (_childIndex == _childCount)
        {
            if (string.IsNullOrEmpty(_initialScenePath) || !ResourceLoader.Exists(_initialScenePath))
            {
                _logger.Warning("Cannot change scene to \"{0}\" because it is empty or does not exist",
                    _initialScenePath);
                return;
            }

            GetTree().ChangeSceneToFile(_initialScenePath);
            return;
        }

        var child = GetChild(_childIndex);
        if (child is VideoStreamPlayer videoStreamPlayer)
        {
            QueueVideoPlayer(videoStreamPlayer);
        }
        else if (child is CanvasItem genericCanvasItem)
        {
            _logger.Debug("Tween in card {0}:{1}", genericCanvasItem.Name, _childIndex);
            genericCanvasItem.SelfModulate = new Color(1f, 1f, 1f, 0f);
            genericCanvasItem.Visible = true;
            var tweenIn = GetTree().CreateTween();
            tweenIn.TweenProperty(
                genericCanvasItem,
                "self_modulate",
                Colors.White,
                _staticImageFadeInTimeInSeconds);
            tweenIn.TweenCallback(Callable.From(() =>
            {
                _logger.Debug("Hold card {0}:{1}", genericCanvasItem.Name, _childIndex);
                var timer = GetTree().CreateTimer(_staticImageLingerTimeInSeconds);
                timer.Timeout += () =>
                {
                    _logger.Debug("Tween out card {0}:{1}", genericCanvasItem.Name, _childIndex);
                    var tweenOut = GetTree().CreateTween();
                    tweenOut.TweenProperty(
                        genericCanvasItem,
                        "self_modulate",
                        new Color(1f, 1f, 1f, 0f),
                        _staticImageFadeOutTimeInSeconds);
                    tweenOut.TweenCallback(Callable.From(() =>
                    {
                        _logger.Debug("Next slide ({0}), please", _childIndex);
                        _childIndex++;
                        StartNextSlide();
                    }));
                };
            }));
        }
    }

    private void QueueVideoPlayer(VideoStreamPlayer videoStreamPlayer)
    {
        _logger.Debug("Play video card {0}:{1}", videoStreamPlayer.Name, _childIndex);
        videoStreamPlayer.Finished += () =>
        {
            _logger.Debug("Tween out video {0}:{1}", videoStreamPlayer.Name, _childIndex);
            var tweenOut = GetTree().CreateTween();
            tweenOut.TweenProperty(
                videoStreamPlayer,
                "self_modulate",
                new Color(1f, 1f, 1f, 0f),
                _staticImageFadeOutTimeInSeconds);
            tweenOut.TweenCallback(Callable.From(() =>
            {
                _logger.Debug("Next slide ({0}), please", _childIndex);
                _childIndex++;
                StartNextSlide();
            }));
        };
        videoStreamPlayer.Visible = true;
        videoStreamPlayer.Paused = false;
        videoStreamPlayer.Play();
    }
}