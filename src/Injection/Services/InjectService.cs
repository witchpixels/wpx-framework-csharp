using System;

namespace Witchpixels.Framework.Injection;

[AttributeUsage(AttributeTargets.Field)]
public class InjectService : Attribute
{
}