using System;
using System.Collections.Generic;

namespace Witchpixels.Framework.Injection;

public static class GlobalServiceContainer
{
    private static readonly Dictionary<Type, Func<object>> ServiceRegistry = new();

    public static void RegisterService<T>(Func<T> serviceFactory)
    {
        ServiceRegistry[typeof(T)] = () => serviceFactory();
    }

    public static void RegisterService<T>(T serviceInstance)
    {
        ServiceRegistry[typeof(T)] = () => serviceInstance;
    }

    public static object ResolveService(Type type)
    {
        if (TryResolveService(type, out var service)) return service;

        throw new Exception($"Missing Service Entry for {type.Name}");
    }

    public static T ResolveService<T>()
    {
        var serviceObj = ResolveService(typeof(T));
        if (serviceObj is T service) return service;
        throw new InvalidCastException($"Cannot cast {serviceObj.GetType().Name} to requested {typeof(T)}");
    }

    public static bool TryResolveService(Type type, out object service)
    {
        if (ServiceRegistry.TryGetValue(type, out var serviceFunc))
        {
            service = serviceFunc();
            return true;
        }

        service = default;
        return false;
    }

    public static bool TryResolveService<T>(out T service)
    {
        if (TryResolveService(typeof(T), out var serviceObj))
        {
            service = (T)serviceObj;
            return false;
        }

        service = default;
        return false;
    }
}