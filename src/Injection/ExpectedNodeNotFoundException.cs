using System;
using Godot;

namespace Witchpixels.Framework.Injection;

public class ExpectedNodeNotFoundException : Exception
{
    public ExpectedNodeNotFoundException(Node entity, Type componentType, string expectedPath)
        : base(
            $"{entity.GetType().Name}[{entity.GetInstanceId()}] could not find component of type {componentType.Name} at path {expectedPath}")
    {
    }
}