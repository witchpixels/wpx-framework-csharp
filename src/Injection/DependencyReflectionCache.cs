using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Godot;
using Witchpixels.Framework.Diagnostics.Logging;

namespace Witchpixels.Framework.Injection;

public static class DependencyReflectionCache
{
    private static readonly Dictionary<Type, DependencyInfo> DependenciesByType = new();

    public static DependencyInfo GetDependencies(Type entityType)
    {
        if (DependenciesByType.TryGetValue(entityType, out var dependencyInfo)) return dependencyInfo;

        var components = new List<ComponentDependency>();
        var dependencies = new List<Dependency>();
        Dependency logger = null;

        foreach (var field in entityType.GetFields(BindingFlags.FlattenHierarchy | BindingFlags.Instance |
                                                   BindingFlags.Public | BindingFlags.NonPublic))
        {
            if (field.FieldType == typeof(ILogger))
            {
                logger = new Dependency(
                    field.FieldType,
                    (i, v) => field.SetValue(i, v));
                continue;
            }

            var attribute = field.GetCustomAttributes(true)
                .ToArray();

            foreach (var componentNeed in attribute.OfType<RequireChild>())
                components.Add(new ComponentDependency(
                    componentNeed.GetPath(field.FieldType),
                    field.FieldType,
                    (node, v) => field.SetValue(node, v)));

            foreach (var _ in attribute.OfType<InjectService>())
                dependencies.Add(new Dependency(
                    field.FieldType,
                    (i, v) => field.SetValue(i, v)));
        }

        dependencyInfo = new DependencyInfo(
            logger,
            dependencies,
            components);

        if (!Engine.IsEditorHint())
            DependenciesByType[entityType] = dependencyInfo;

        return dependencyInfo;
    }


    public class DependencyInfo
    {
        public IReadOnlyList<ComponentDependency> Components;
        public IReadOnlyList<Dependency> Dependencies;
        public Dependency Logger;

        public DependencyInfo(
            Dependency logger,
            IReadOnlyList<Dependency> dependencies,
            IReadOnlyList<ComponentDependency> components)
        {
            Logger = logger;
            Dependencies = dependencies;
            Components = components;
        }
    }

    public class Dependency
    {
        public readonly Type DependencyType;
        public readonly Action<object, object> SetFunction;

        public Dependency(Type dependencyType, Action<object, object> setFunction)
        {
            DependencyType = dependencyType;
            SetFunction = setFunction;
        }
    }

    public class ComponentDependency
    {
        public readonly string ComponentPath;
        public readonly Type ComponentType;
        public readonly Action<Node, object> SetFunction;

        public ComponentDependency(string componentPath, Type componentType, Action<Node, object> setFunction)
        {
            ComponentPath = componentPath;
            ComponentType = componentType;
            SetFunction = setFunction;
        }
    }
}