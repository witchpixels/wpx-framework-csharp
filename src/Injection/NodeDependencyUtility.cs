using System;
using System.Collections.Generic;
using System.Linq;
using Godot;
using Witchpixels.Framework.Diagnostics.Logging;

namespace Witchpixels.Framework.Injection;

public static class NodeDependencyUtility
{
    public static void ResolveDependencies(Node node)
    {
        if (Engine.IsEditorHint()) return;

        var dependencyInfo = DependencyReflectionCache.GetDependencies(node.GetType());
        foreach (var component in dependencyInfo.Components)
            component.SetFunction(node, ResolveDependency(node, component.ComponentType, component.ComponentPath));

        foreach (var dependency in dependencyInfo.Dependencies)
            dependency.SetFunction(node, GlobalServiceContainer.ResolveService(dependency.DependencyType));

        if (dependencyInfo.Logger != null)
        {
            var logFactory = GlobalServiceContainer.ResolveService<ILoggerFactory>();
            dependencyInfo.Logger.SetFunction(node, logFactory.CreateInstanceLogger(node));
        }
    }

    public static Node ResolveDependency(Node node, Type componentType, string componentPath)
    {
#if DEBUG
        if (!HasDependency(node, componentType, componentPath))
            throw new ExpectedNodeNotFoundException(node, componentType, componentPath);
#endif

        return node.GetNode($"{componentPath}");
    }

    private static bool HasDependency(Node node, Type componentType, string componentPath)
    {
        return node.HasNode($"{componentPath}")
               && (node.GetNode(componentPath).GetType() == componentType
                   || node.GetNode(componentPath).GetType().IsSubclassOf(componentType));
    }


    public static IEnumerable<string> GetDependencyConfigurationWarnings(Node node)
    {
        var dependencyInfo = DependencyReflectionCache.GetDependencies(node.GetType());
        return dependencyInfo.Components.Where(x => !HasDependency(node, x.ComponentType, x.ComponentPath))
            .Select(x =>
                $"Missing component of type {x.ComponentType.Name} at {x.ComponentPath}");
    }
}