using System;

namespace Witchpixels.Framework.Injection;

[AttributeUsage(AttributeTargets.Field)]
public class RequireComponentAttribute : RequireChild
{
    public RequireComponentAttribute(string path = null)
        : base(path)
    {
    }

    public override string GetPath(Type fieldType)
    {
        return $"components/{base.GetPath(fieldType)}";
    }
}