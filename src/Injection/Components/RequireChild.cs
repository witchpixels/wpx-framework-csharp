using System;

namespace Witchpixels.Framework.Injection;

[AttributeUsage(AttributeTargets.Field)]
public class RequireChild : Attribute
{
    public RequireChild(string path = null)
    {
        Path = path;
    }

    public string Path { get; }

    public virtual string GetPath(Type fieldType)
    {
        return Path ?? fieldType.Name;
    }
}