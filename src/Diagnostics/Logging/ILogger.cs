using System;
using System.Collections.Generic;

namespace Witchpixels.Framework.Diagnostics.Logging;

public interface ILogger
{
    void Log(LogLevel logLevel, string format, params object[] args);
}

public class Logger : ILogger
{
    private readonly LogLevel _logLevel;
    private readonly IReadOnlyList<ILogTarget> _logTargets;
    private readonly string _prefix;

    private readonly IReadOnlyDictionary<LogLevel, string> LogLevelShortCodeMap = new Dictionary<LogLevel, string>
    {
        { LogLevel.Debug, "DBG" },
        { LogLevel.Info, "INF" },
        { LogLevel.Warning, "WRN" },
        { LogLevel.Error, "ERR" },
        { LogLevel.Fatal, "FTL" }
    };

    public Logger(
        IReadOnlyList<ILogTarget> logTargets,
        LogLevel logLevel,
        string prefix)
    {
        _logTargets = logTargets;
        _logLevel = logLevel;
        _prefix = prefix;
    }

    public void Log(LogLevel logLevel, string format, params object[] args)
    {
        if (logLevel >= _logLevel)
        {
            var s = $"{DateTime.Now:u} {LogLevelShortCodeMap[logLevel]} {_prefix} {string.Format(format, args)}";
            foreach (var logTarget in _logTargets) logTarget.Log(logLevel, s);
        }
    }
}