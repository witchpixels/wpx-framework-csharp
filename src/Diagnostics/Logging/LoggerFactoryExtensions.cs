namespace Witchpixels.Framework.Diagnostics.Logging;

public static class LoggerFactoryExtensions
{
    public static ILogger CreateServiceLogger<T>(this ILoggerFactory loggerFactory)
    {
        return loggerFactory.CreateServiceLogger(typeof(T));
    }
}