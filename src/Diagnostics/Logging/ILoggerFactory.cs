using System;
using Godot;

namespace Witchpixels.Framework.Diagnostics.Logging;

public interface ILoggerFactory
{
    ILogger CreateServiceLogger(Type type);
    ILogger CreateInstanceLogger(Node node);
}