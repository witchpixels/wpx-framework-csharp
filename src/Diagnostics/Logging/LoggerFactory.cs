using System;
using System.Collections.Generic;
using Godot;

namespace Witchpixels.Framework.Diagnostics.Logging;

public class LoggerFactory : ILoggerFactory
{
    private readonly LogLevel _logLevel;
    private readonly IReadOnlyList<ILogTarget> _logTargets;

    public LoggerFactory(LogLevel logLevel, IReadOnlyList<ILogTarget> logTargets)
    {
        _logLevel = logLevel;
        _logTargets = logTargets;
    }

    public ILogger CreateServiceLogger(Type type)
    {
        return new Logger(_logTargets, _logLevel, $"[{type.Name}]:");
    }

    public ILogger CreateInstanceLogger(Node node)
    {
        return new Logger(_logTargets, _logLevel, $"[{node.GetType().Name}:{node.Name}]:");
    }
}