namespace Witchpixels.Framework.Diagnostics.Logging;

public interface ILogTarget
{
    public void Log(LogLevel logLevel, string s);
}