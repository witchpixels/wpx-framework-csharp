namespace Witchpixels.Framework.Diagnostics.Logging.Targets;

public class DummyLogger : ILogger
{
    public static DummyLogger Default { get; } = new();

    public void Log(LogLevel logLevel, string format, params object[] args)
    {
    }
}