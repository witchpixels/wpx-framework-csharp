using Godot;

namespace Witchpixels.Framework.Diagnostics.Logging.Targets;

public class GdPrintLogTarget : ILogTarget
{
    public void Log(LogLevel logLevel, string s)
    {
        switch (logLevel)
        {
            case LogLevel.Debug:
                GD.Print(s);
                break;
            case LogLevel.Info:
                GD.Print(s);
                break;
            case LogLevel.Warning:
                GD.Print(s);
                GD.PushWarning(s);
                break;
            case LogLevel.Error:
                GD.PrintErr(s);
                GD.PushError(s);
                break;
            case LogLevel.Fatal:
                GD.PrintErr(s);
                GD.PushError(s);
                break;
        }
    }
}