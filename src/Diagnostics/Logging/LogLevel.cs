namespace Witchpixels.Framework.Diagnostics.Logging;

public enum LogLevel
{
    Debug,
    Info,
    Warning,
    Error,
    Fatal
}