using System.Collections.Generic;
using System.Linq;
using Godot;
using Witchpixels.Framework.Injection;

namespace Witchpixels.Framework.Entities;

public static class NodeInjectionExtensions
{
    public static void ResolveDependencies<TNodeRoot>(this TNodeRoot node)
        where TNodeRoot : Node, new()
    {
        EntityUtility.EnsureComponentRootExists<TNodeRoot>(node);
        NodeDependencyUtility.ResolveDependencies(node);
    }

    public static string[] GetConfigurationWarnings(this Node node)
    {
        return NodeDependencyUtility.GetDependencyConfigurationWarnings(node).ToArray();
    }

    public static string[] ConcatConfigurationWarnings(this Node node, IEnumerable<string> warnings)
    {
        return warnings != null
            ? warnings.Concat(NodeDependencyUtility.GetDependencyConfigurationWarnings(node)).ToArray()
            : NodeDependencyUtility.GetDependencyConfigurationWarnings(node).ToArray();
    }
}