using System;
using Godot;

namespace Witchpixels.Framework.Entities;

[Tool]
[GlobalClass]
[Icon("res://addons/Witchpixels.Framework/src/Entities/node_entity_icon.png")]
public partial class NodeEntity : Node
{
    public override void _Ready()
    {
        this.ResolveDependencies<Node>();
        base._Ready();
    }

    public override string[] _GetConfigurationWarnings()
    {
        return this.ConcatConfigurationWarnings(base._GetConfigurationWarnings() ?? Array.Empty<string>());
    }
}