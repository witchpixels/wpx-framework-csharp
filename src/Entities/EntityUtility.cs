using Godot;

namespace Witchpixels.Framework.Entities;

public static class EntityUtility
{
    public static void EnsureComponentRootExists<TNodeClass>(Node entity)
        where TNodeClass : Node, new()
    {
        if (Engine.IsEditorHint() && !entity.HasNode("components"))
        {
            var node = new TNodeClass();
            node.Name = "components";
            entity.AddChild(node);
            node.Owner = entity.GetTree().EditedSceneRoot;
        }
    }
}