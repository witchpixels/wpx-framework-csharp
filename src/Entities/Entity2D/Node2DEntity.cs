using Godot;

namespace Witchpixels.Framework.Entities.Entity2D;

[Tool]
[GlobalClass]
[Icon("res://addons/Witchpixels.Framework/src/Entities/node2d_entity_icon.png")]
public partial class Node2DEntity : Node2D
{
    public override void _Ready()
    {
        this.ResolveDependencies<Node2D>();
        base._Ready();
    }

    public override string[] _GetConfigurationWarnings()
    {
        return this.ConcatConfigurationWarnings(base._GetConfigurationWarnings());
    }
}