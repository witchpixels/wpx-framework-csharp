using Godot;

namespace Witchpixels.Framework.Entities.Entity3D;

[Tool]
[GlobalClass]
[Icon("res://addons/Witchpixels.Framework/src/Entities/node3d_entity_icon.png")]
public partial class Character3DEntity : CharacterBody3D
{
    public override void _Ready()
    {
        this.ResolveDependencies<Node3D>();
        base._Ready();
    }

    public override string[] _GetConfigurationWarnings()
    {
        return this.ConcatConfigurationWarnings(base._GetConfigurationWarnings());
    }
}