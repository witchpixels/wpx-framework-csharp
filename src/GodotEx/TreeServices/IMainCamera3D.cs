using Godot;

namespace Witchpixels.Framework.GodotEx.TreeServices;

public interface IMainCamera3D
{
    public Camera3D GetMainCamera();

    public Vector2 WorldToScreenPosition(Vector3 globalPosition);
    public Vector3 ScreenToWorldPosition(Vector2 screenPosition, float maxDepth = 1000f);
}