using Godot;
using Witchpixels.Framework.Initialization;

namespace Witchpixels.Framework.GodotEx.TreeServices;

public partial class MainCamera3D : Node3D, IMainCamera3D, IWaitOnReadyNode
{
    public Camera3D GetMainCamera()
    {
        return GetViewport().GetCamera3D();
    }

    public Vector2 WorldToScreenPosition(Vector3 globalPosition)
    {
        return GetMainCamera().UnprojectPosition(globalPosition);
    }

    public Vector3 ScreenToWorldPosition(Vector2 screenPosition, float maxDepth)
    {
        var camera = GetMainCamera();

        var rayNormal = camera.ProjectRayNormal(screenPosition);
        var rayOrigin = camera.ProjectRayOrigin(screenPosition);

        var rayCast = PhysicsRayQueryParameters3D.Create(rayOrigin, rayNormal * maxDepth);

        var rayCastResult = GetWorld3D().DirectSpaceState.IntersectRay(rayCast);

        return rayCastResult.TryGetValue("position", out var pos)
            ? pos.AsVector3()
            : Vector3.Inf;
    }

    public string TargetName => "MainCamera3D";
    public bool IsReady { get; set; }

    public override void _Ready()
    {
        base._Ready();
        IsReady = true;
    }
}