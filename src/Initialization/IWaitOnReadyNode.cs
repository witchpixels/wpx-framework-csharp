namespace Witchpixels.Framework.Initialization;

public interface IWaitOnReadyNode
{
    public string TargetName { get; }
    public bool IsReady { get; protected set; }
}