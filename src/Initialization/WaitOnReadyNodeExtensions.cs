using System.Diagnostics;
using System.Threading.Tasks;
using Witchpixels.Framework.Diagnostics.Logging;

namespace Witchpixels.Framework.Initialization;

public static class WaitOnReadyNodeExtensions
{
    public static async Task WaitOnReady(this IWaitOnReadyNode node, ILogger logger)
    {
        var stopwatch = new Stopwatch();
        stopwatch.Start();
        while (!node.IsReady)
        {
            await Task.Delay(1);
            logger.Info("waiting on {0} for {1}ms",
                node.TargetName,
                stopwatch.Elapsed.TotalMilliseconds);
        }
    }
}