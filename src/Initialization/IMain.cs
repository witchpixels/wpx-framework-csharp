using Godot;
using Witchpixels.Framework.Diagnostics.Logging;

namespace Witchpixels.Framework.Initialization;

public interface IMain
{
    ILogger Logger { get; }

    void AddServiceChild(Node serviceNode);
}