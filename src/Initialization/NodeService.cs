using System;
using System.Threading.Tasks;
using Godot;
using Witchpixels.Framework.Diagnostics.Logging;

namespace Witchpixels.Framework.Initialization;

public static class NodeService
{
    public static async Task<T> Create<T>(this IMain main, Func<T> factoryFunc)
        where T : Node, IWaitOnReadyNode
    {
        main.Logger.Debug("Create {0}...", typeof(T).Name);

        var node = factoryFunc();
        return await AddChildToMain(main, node);
    }

    public static async Task<T> CreateFromPackedScene<T>(this IMain main, string path)
        where T : Node, IWaitOnReadyNode
    {
        main.Logger.Debug("Create {0} from {1}...", typeof(T).Name, path);
        var scene = GD.Load<PackedScene>(path);

        if (scene == null)
        {
            main.Logger.Error("No {0} could be found at {1}", typeof(T).Name, path);
            return null;
        }

        var node = scene.InstantiateOrNull<T>();

        if (node == null)
        {
            main.Logger.Error("Unable to instantiate {1} into type {0}", typeof(T).Name, path);
            return null;
        }

        return await AddChildToMain(main, node);
    }

    private static async Task<T> AddChildToMain<T>(IMain main, T node)
        where T : Node, IWaitOnReadyNode
    {
        main.AddServiceChild(node);
        await node.WaitOnReady(main.Logger);
        return node;
    }
}